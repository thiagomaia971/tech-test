﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using ZombieSources.Infra;
using ZombieSources.Core.Enums;

namespace ZombieSources.Infra.Migrations
{
    [DbContext(typeof(ZombieSourcesContext))]
    [Migration("20170620010838_InitialMIgration")]
    partial class InitialMIgration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("ZombieSources.Core.Entities.Recurso", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Descricao")
                        .IsRequired()
                        .HasMaxLength(100);

                    b.Property<string>("Obervação")
                        .HasMaxLength(500);

                    b.Property<int>("Quantidade");

                    b.HasKey("Id");

                    b.ToTable("Recurso");
                });

            modelBuilder.Entity("ZombieSources.Core.Entities.RecursoMovimento", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("RecursoId");

                    b.Property<string>("Responsavel");

                    b.Property<int>("TipoMovimento");

                    b.HasKey("Id");

                    b.HasIndex("RecursoId");

                    b.ToTable("RecursosMovimento");
                });

            modelBuilder.Entity("ZombieSources.Core.Entities.RecursoMovimento", b =>
                {
                    b.HasOne("ZombieSources.Core.Entities.Recurso", "Recurso")
                        .WithMany("RecursosMovimento")
                        .HasForeignKey("RecursoId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
