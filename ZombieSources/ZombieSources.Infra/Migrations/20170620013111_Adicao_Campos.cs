﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ZombieSources.Infra.Migrations
{
    public partial class Adicao_Campos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RecursosMovimento_Recurso_RecursoId",
                table: "RecursosMovimento");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RecursosMovimento",
                table: "RecursosMovimento");

            migrationBuilder.RenameTable(
                name: "RecursosMovimento",
                newName: "RecursoMovimento");

            migrationBuilder.RenameIndex(
                name: "IX_RecursosMovimento_RecursoId",
                table: "RecursoMovimento",
                newName: "IX_RecursoMovimento_RecursoId");

            migrationBuilder.AlterColumn<string>(
                name: "Responsavel",
                table: "RecursoMovimento",
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Data",
                table: "RecursoMovimento",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "Quantidade",
                table: "RecursoMovimento",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_RecursoMovimento",
                table: "RecursoMovimento",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_RecursoMovimento_Recurso_RecursoId",
                table: "RecursoMovimento",
                column: "RecursoId",
                principalTable: "Recurso",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RecursoMovimento_Recurso_RecursoId",
                table: "RecursoMovimento");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RecursoMovimento",
                table: "RecursoMovimento");

            migrationBuilder.DropColumn(
                name: "Data",
                table: "RecursoMovimento");

            migrationBuilder.DropColumn(
                name: "Quantidade",
                table: "RecursoMovimento");

            migrationBuilder.RenameTable(
                name: "RecursoMovimento",
                newName: "RecursosMovimento");

            migrationBuilder.RenameIndex(
                name: "IX_RecursoMovimento_RecursoId",
                table: "RecursosMovimento",
                newName: "IX_RecursosMovimento_RecursoId");

            migrationBuilder.AlterColumn<string>(
                name: "Responsavel",
                table: "RecursosMovimento",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 100);

            migrationBuilder.AddPrimaryKey(
                name: "PK_RecursosMovimento",
                table: "RecursosMovimento",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_RecursosMovimento_Recurso_RecursoId",
                table: "RecursosMovimento",
                column: "RecursoId",
                principalTable: "Recurso",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
