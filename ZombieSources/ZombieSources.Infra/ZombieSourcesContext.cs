﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using ZombieSources.Core.Entities;
using ZombieSources.Infra.EntityTypes;

namespace ZombieSources.Infra
{
    public class ZombieSourcesContext : DbContext
    {
        DbSet<Recurso> Recursos { get; }
        DbSet<RecursoMovimento> RecursosMovimento { get; }

        public ZombieSourcesContext(DbContextOptions<ZombieSourcesContext> options): base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.AddConfiguration(new RecursoTypeConfig());
            modelBuilder.AddConfiguration(new RecursoMovimentoTypeConfig());
        }

    }
}
