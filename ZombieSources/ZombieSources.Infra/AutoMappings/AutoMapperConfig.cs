﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace ZombieSources.Infra.AutoMappings
{
    public static class AutoMapperConfig
    {
        public static void Register()
        {
            // Todos os Profiles.
            Mapper.Initialize(m =>
            {
                m.AddProfile<RecursoMapperConfig>();
                m.AddProfile<RecursoMovimentoMapperConfig>();
            });

            //Mapper.AssertConfigurationIsValid();
        }
    }
}
