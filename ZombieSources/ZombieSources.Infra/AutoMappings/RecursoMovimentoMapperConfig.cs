﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using ZombieSources.Core.Entities;
using ZombieSources.Core.ViewModels;

namespace ZombieSources.Infra.AutoMappings
{
    public class RecursoMovimentoMapperConfig : Profile
    {
        public RecursoMovimentoMapperConfig()
        {
            CreateMap<RecursoMovimento, RecursoMovimentoListaViewModel>().ReverseMap();
            CreateMap<RecursoMovimento, RecursoMovimentoCriarViewModel>().ReverseMap();
        }
    }
}
