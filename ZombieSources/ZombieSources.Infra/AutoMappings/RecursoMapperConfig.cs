﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using ZombieSources.Core.Entities;
using ZombieSources.Core.ViewModels;

namespace ZombieSources.Infra.AutoMappings
{
    public class RecursoMapperConfig : Profile
    {
        public RecursoMapperConfig()
        {
            CreateMap<Recurso, RecursoCriarViewModel>()
                .ReverseMap();
        }
    }
}
