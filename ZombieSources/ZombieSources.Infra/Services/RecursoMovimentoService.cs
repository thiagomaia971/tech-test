﻿using AutoMapper;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZombieSources.Core.Entities;
using ZombieSources.Core.Enums;
using ZombieSources.Core.Interfaces;
using ZombieSources.Core.Interfaces.Services;
using ZombieSources.Core.ViewModels;

namespace ZombieSources.Infra.Services
{
    public class RecursoMovimentoService : IRecursoMovimentoService
    {

        private readonly IUnitOfWork UnitOfWork;

        public RecursoMovimentoService(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }


        public IEnumerable<RecursoMovimento> BuscarTodos()
        {
            return this.UnitOfWork.RecursosMovimento.GetAllWithInclude().OrderByDescending(x => x.Id);

        }

        public void Cadastrar(RecursoMovimentoCriarViewModel movimento)
        {
            var recurso = this.UnitOfWork.Recursos.GetSingle(movimento.RecursoId);

            if (recurso == null)
                throw new Exception("Recurso não existe.");

            switch (movimento.TipoMovimento)
            {
                case TipoMovimento.ENTRADA:
                    recurso.Quantidade += movimento.Quantidade;
                    break;

                case TipoMovimento.SAIDA:
                    if (recurso.Quantidade < movimento.Quantidade)
                        throw new Exception("Saldo menor que a quantidade pedida para retirada.");

                    recurso.Quantidade -= movimento.Quantidade;
                    break;
                default:
                    break;
            }


            RecursoMovimento movimentoToCreate = new RecursoMovimento();
            Mapper.Map(movimento, movimentoToCreate);

            this.UnitOfWork.Recursos.Update(recurso);
            this.UnitOfWork.RecursosMovimento.Add(movimentoToCreate);

            this.UnitOfWork.SaveChanges();

        }

        public IEnumerable<RecursoMovimento> Filtrar(int recursoId, TipoMovimento? tipoMovimento, string responsavel)
        {
            var predicate = PredicateBuilder.New<RecursoMovimento>(false);

            if (recursoId > 0)
                predicate = predicate.And(x => x.RecursoId == recursoId);

            if (tipoMovimento.HasValue)
                predicate = predicate.And(x => x.TipoMovimento == tipoMovimento.Value);

            if (!string.IsNullOrEmpty(responsavel))
                predicate = predicate.And(x => x.Responsavel.ToLower().Contains(responsavel.ToLower()));

            var recursos = this.UnitOfWork.RecursosMovimento.GetAllWithInclude(predicate).OrderByDescending(x => x.Id);

            return recursos;
        }
    }
}
