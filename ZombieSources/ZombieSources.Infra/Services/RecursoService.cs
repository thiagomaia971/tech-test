﻿using AutoMapper;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Text;
using ZombieSources.Core.Entities;
using ZombieSources.Core.Interfaces;
using ZombieSources.Core.Interfaces.Services;
using ZombieSources.Core.ViewModels;
using System.Linq;

namespace ZombieSources.Infra.Services
{
    public class RecursoService : IRecursoService
    {

        private readonly IUnitOfWork UnitOfWork;

        public RecursoService(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        public Recurso BuscarPorId(int id)
        {
            return this.UnitOfWork.Recursos.GetSingle(id);
        }

        public IEnumerable<Recurso> BuscarTodos()
        {
            return this.UnitOfWork.Recursos.GetAll().OrderBy(x => x.Id);
        }

        public void Cadastrar(RecursoCriarViewModel recurso)
        {
            Recurso recursoToRegister = Mapper.Map<RecursoCriarViewModel, Recurso>(recurso);

            this.UnitOfWork.Recursos.Add(recursoToRegister);
            this.UnitOfWork.SaveChanges();
        }

        public void Deletar(int id)
        {
            var recursoToDelete = this.UnitOfWork.Recursos.GetSingle(id);
            if (recursoToDelete == null)
                throw new Exception("Recurso não encontrado");

            this.UnitOfWork.Recursos.Delete(recursoToDelete);
            this.UnitOfWork.SaveChanges();
        }

        public void Editar(int id, RecursoCriarViewModel recurso)
        {
            var recursoToUpdate = this.UnitOfWork.Recursos.GetSingle(id);
            if (recursoToUpdate == null)
                throw new Exception("Recurso não encontrado");

            recursoToUpdate = Mapper.Map(recurso, recursoToUpdate);

            this.UnitOfWork.Recursos.Update(recursoToUpdate);
            this.UnitOfWork.SaveChanges();
        }

        public IEnumerable<Recurso> Filtrar(int id, string descricao)
        {
            var predicate = PredicateBuilder.New<Recurso>(false);

            if (id > 0)
                predicate = predicate.And(x => x.Id == id);

            if (!string.IsNullOrEmpty(descricao))
                predicate = predicate.And(x => x.Descricao.ToLower().Contains(descricao.ToLower()));

            var recursos = this.UnitOfWork.Recursos.GetAll(predicate).OrderBy(x => x.Id);

            return recursos;
        }
    }
}
