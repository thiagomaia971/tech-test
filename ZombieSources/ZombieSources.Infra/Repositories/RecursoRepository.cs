﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using ZombieSources.Core.Entities;
using ZombieSources.Core.Interfaces.Repositories;

namespace ZombieSources.Infra.Repositories
{
    public class RecursoRepository : Repository<Recurso>, IRecursoRepository
    {
        public RecursoRepository(DbContext Context) : base(Context)
        {
        }
    }
}
