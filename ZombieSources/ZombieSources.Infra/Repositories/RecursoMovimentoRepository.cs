﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using ZombieSources.Core.Entities;
using ZombieSources.Core.Interfaces.Repositories;
using System.Linq;

namespace ZombieSources.Infra.Repositories
{
    public class RecursoMovimentoRepository : Repository<RecursoMovimento>, IRecursoMovimentoRepository
    {
        public RecursoMovimentoRepository(DbContext Context) : base(Context)
        {
        }

        public IEnumerable<RecursoMovimento> GetAllWithInclude()
        {
            return this.Context.Set<RecursoMovimento>().Include(x => x.Recurso);
        }

        public IEnumerable<RecursoMovimento> GetAllWithInclude(Func<RecursoMovimento, bool> predicate)
        {
            return this.Context.Set<RecursoMovimento>().Include(x => x.Recurso).Where(predicate);
        }
    }
}
