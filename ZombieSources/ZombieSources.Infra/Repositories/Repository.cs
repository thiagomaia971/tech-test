﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ZombieSources.Core.Entities;
using ZombieSources.Core.Interfaces.Repositories;

namespace ZombieSources.Infra.Repositories
{
    public class Repository<T> : IRepository<T> where T : Entity
    {
        protected readonly DbContext Context;
        private DbSet<T> dbSet;

        public Repository(DbContext Context)
        {
            this.Context = Context;
            dbSet = this.Context.Set<T>();
        }

        public void Add(T Entity)
        {
            dbSet.Add(Entity);
        }

        public void Delete(T Entity)
        {
            dbSet.Remove(Entity);
        }

        public IEnumerable<T> GetAll()
        {
            return dbSet.ToList();
        }

        public IEnumerable<T> GetAll(Func<T, bool> predicate)
        {
            return dbSet.Where(predicate);
        }

        public T GetSingle(int id)
        {
            return dbSet.Find(id);
        }

        public void Update(T Entity)
        {
            //this.Context.Entry(Entity).State = EntityState.Modified;
            dbSet.Update(Entity);
        }

    }
}
