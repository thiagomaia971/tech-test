﻿using System;
using System.Collections.Generic;
using System.Text;
using ZombieSources.Core.Interfaces;
using ZombieSources.Core.Interfaces.Repositories;
using ZombieSources.Infra.Repositories;

namespace ZombieSources.Infra
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {

        private readonly ZombieSourcesContext Context;

        public IRecursoRepository Recursos => new RecursoRepository(Context);
        public IRecursoMovimentoRepository RecursosMovimento => new RecursoMovimentoRepository(Context);

        public UnitOfWork(ZombieSourcesContext context)
        {
            this.Context = context;
        }

        public void SaveChanges()
        {
            Context.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.Context.Dispose();
            }
        }

    }
}
