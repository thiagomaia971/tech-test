﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZombieSources.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace ZombieSources.Infra.EntityTypes
{
    public class RecursoMovimentoTypeConfig : EntityTypeConfiguration<RecursoMovimento>
    {
        public override void Map(EntityTypeBuilder<RecursoMovimento> builder)
        {
            builder.ToTable("RecursoMovimento");

            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id)
                .IsRequired()
                .UseSqlServerIdentityColumn();

            builder.HasOne(x => x.Recurso)
                .WithMany(x => x.RecursosMovimento)
                .HasForeignKey(x => x.RecursoId)
                .IsRequired();

            builder.Property(x => x.Responsavel).IsRequired().HasMaxLength(100);
            builder.Property(x => x.TipoMovimento).IsRequired();
            builder.Property(x => x.Data).IsRequired();
            builder.Property(x => x.Quantidade).IsRequired();
        }
    }
}
