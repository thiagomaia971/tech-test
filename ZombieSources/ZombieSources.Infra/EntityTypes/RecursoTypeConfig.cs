﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ZombieSources.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace ZombieSources.Infra.EntityTypes
{
    public class RecursoTypeConfig : EntityTypeConfiguration<Recurso>
    {
        public override void Map(EntityTypeBuilder<Recurso> builder)
        {
            builder.ToTable("Recurso");

            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id)
                .IsRequired()
                .UseSqlServerIdentityColumn();

            builder.Property(x => x.Descricao).IsRequired().HasMaxLength(100);
            builder.Property(x => x.Observacao).HasMaxLength(500);
            builder.Property(x => x.Quantidade).IsRequired();
        }
    }
}
