﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using ZombieSources.Core.Entities;

namespace ZombieSources.Infra.EntityTypes
{
    public abstract class EntityTypeConfiguration<TEntity> where TEntity : Entity
    {
        public abstract void Map(EntityTypeBuilder<TEntity> builder);
    }

    public static class ModelBuilderExtensions
    {
        public static void AddConfiguration<TEntity>(this ModelBuilder modelBuilder, EntityTypeConfiguration<TEntity> configuration) where TEntity : Entity
        {
            configuration.Map(modelBuilder.Entity<TEntity>());
        }
    }

}
