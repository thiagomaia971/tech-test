﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ZombieSources.Core.Interfaces;
using ZombieSources.Core.Entities;
using LinqKit;
using ZombieSources.Core.Interfaces.Services;
using ZombieSources.Core.ViewModels;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ZombieSources.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    public class RecursosController : Controller
    {

        private readonly IUnitOfWork UnitOfWork;
        private readonly IRecursoService RecursoService;

        public RecursosController(IUnitOfWork unitOfWork, IRecursoService recursoService)
        {
            UnitOfWork = unitOfWork;
            RecursoService = recursoService;
        }

        // GET: api/values
        [HttpGet]
        public IActionResult Buscar()
        {
            IEnumerable<Recurso> recursos = null;
            try
            {
                recursos = this.RecursoService.BuscarTodos();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(recursos);
        }

        [HttpGet]
        public IActionResult Filtrar(int id, string descricao)
        {
            IEnumerable<Recurso> recursos = null;
            try
            {
                recursos = this.RecursoService.Filtrar(id, descricao);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
            return Ok(recursos);
        }

        [HttpGet]
        public IActionResult BuscarPorId(int id)
        {
            Recurso recurso = null;
            try
            {
                recurso = this.RecursoService.BuscarPorId(id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok(recurso);
        }

        [HttpPut]
        public IActionResult Editar(int id, [FromBody] RecursoCriarViewModel recurso)
        {
            try
            {
                this.RecursoService.Editar(id, recurso);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }

        [HttpPost]
        public IActionResult Cadastrar([FromBody] RecursoCriarViewModel recurso)
        {
            if (!ModelState.IsValid)
                return BadRequest("Recurso não está válido.");

            try
            {
                this.RecursoService.Cadastrar(recurso);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return Ok();

        }

        [HttpDelete]
        public IActionResult Deletar(int id)
        {
            try
            {
                this.RecursoService.Deletar(id);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }
        
    }
}
