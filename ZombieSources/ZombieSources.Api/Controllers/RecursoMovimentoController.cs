using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ZombieSources.Core.Interfaces;
using ZombieSources.Core.Interfaces.Services;
using ZombieSources.Core.Enums;
using ZombieSources.Core.Entities;
using AutoMapper;
using ZombieSources.Core.ViewModels;

namespace ZombieSources.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    public class RecursoMovimentoController : Controller
    {
        private readonly IUnitOfWork UnitOfWork;
        private readonly IRecursoMovimentoService RecursoMovimentoService;

        public RecursoMovimentoController(IUnitOfWork unitOfWork, IRecursoMovimentoService recursoMovimentoService)
        {
            UnitOfWork = unitOfWork;
            RecursoMovimentoService = recursoMovimentoService;
        }

        [HttpGet]
        public IActionResult Buscar()
        {
            IEnumerable<RecursoMovimentoListaViewModel> movimentos = new List<RecursoMovimentoListaViewModel>();
            try
            {
                Mapper.Map(this.RecursoMovimentoService.BuscarTodos(), movimentos);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

            return Ok(movimentos.ToList());
        }

        [HttpGet]
        public IActionResult Filtrar(int recursoId, TipoMovimento? tipoMovimento, string responsavel)
        {
            IEnumerable<RecursoMovimentoListaViewModel> movimentos = new List<RecursoMovimentoListaViewModel>();
            try
            {
                Mapper.Map(this.RecursoMovimentoService.Filtrar(recursoId, tipoMovimento, responsavel), movimentos);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

            return Ok(movimentos.ToList());
        }

        [HttpPost]
        public IActionResult Cadastrar([FromBody] RecursoMovimentoCriarViewModel movimento)
        {
            if (!ModelState.IsValid)
                return BadRequest(new { Message = "Movimento n�o est� v�lido." });

            try
            {
                this.RecursoMovimentoService.Cadastrar(movimento);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok();

        }


    }
}