﻿using System;
using System.Collections.Generic;
using System.Text;
using ZombieSources.Core.Enums;

namespace ZombieSources.Core.ViewModels
{
    public class RecursoMovimentoCriarViewModel
    {
        public int RecursoId { get; set; }
        public TipoMovimento TipoMovimento { get; set; }
        public string Responsavel { get; set; }
        public int Quantidade { get; set; }
        public DateTime Data { get; set; }
    }
}
