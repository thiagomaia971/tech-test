﻿using System;
using System.Collections.Generic;
using System.Text;
using ZombieSources.Core.Entities;
using ZombieSources.Core.Enums;

namespace ZombieSources.Core.ViewModels
{
    public class RecursoMovimentoListaViewModel
    {
        public int Id { get; set; }
        public int RecursoId { get; set; }
        public string RecursoDescricao { get; set; }
        public string Responsavel { get; set; }

        public TipoMovimento TipoMovimento { get; set; }
        public int Quantidade { get; set; }
        public int RecursoQuantidade { get; set; }

        public DateTime Data { get; set; }

    }
}
