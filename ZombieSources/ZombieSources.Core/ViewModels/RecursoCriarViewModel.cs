﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ZombieSources.Core.ViewModels
{
    public class RecursoCriarViewModel
    {
        [Required]
        public string Descricao { get; set; }

        public int Quantidade { get; set; }
        public string Observacao { get; set; }
    }
}
