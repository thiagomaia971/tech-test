﻿using System;
using System.Collections.Generic;
using System.Text;
using ZombieSources.Core.Enums;

namespace ZombieSources.Core.Entities
{
    public class RecursoMovimento : Entity
    {
        public int RecursoId { get; set; }
        public TipoMovimento TipoMovimento { get; set; }
        // Para um maior controle, criar a tabela de Sobrevivente.
        public string Responsavel { get; set; }
        public int Quantidade { get; set; }
        public DateTime Data { get; set; }

        public virtual Recurso Recurso { get; set; }
    }
}
