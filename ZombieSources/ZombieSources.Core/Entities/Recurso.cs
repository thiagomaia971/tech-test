﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZombieSources.Core.Entities
{
    public class Recurso : Entity
    {
        public string Descricao { get; set; }
        public int Quantidade { get; set; }
        public string Observacao { get; set; }

        public virtual ICollection<RecursoMovimento> RecursosMovimento { get; set; }
    }
}
