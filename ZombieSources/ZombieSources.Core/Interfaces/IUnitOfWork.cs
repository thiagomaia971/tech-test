﻿using System;
using System.Collections.Generic;
using System.Text;
using ZombieSources.Core.Entities;
using ZombieSources.Core.Interfaces.Repositories;

namespace ZombieSources.Core.Interfaces
{
    public interface IUnitOfWork
    {
        IRecursoRepository Recursos { get; }
        IRecursoMovimentoRepository RecursosMovimento { get; }

        void SaveChanges();
    }
}
