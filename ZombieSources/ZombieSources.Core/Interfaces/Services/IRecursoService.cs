﻿using System;
using System.Collections.Generic;
using System.Text;
using ZombieSources.Core.Entities;
using ZombieSources.Core.ViewModels;

namespace ZombieSources.Core.Interfaces.Services
{
    public interface IRecursoService
    {
        IEnumerable<Recurso> BuscarTodos();
        IEnumerable<Recurso> Filtrar(int id, string descricao);
        Recurso BuscarPorId(int id);
        void Editar(int id, RecursoCriarViewModel recurso);
        void Cadastrar(RecursoCriarViewModel recurso);
        void Deletar(int id);
    }
}
