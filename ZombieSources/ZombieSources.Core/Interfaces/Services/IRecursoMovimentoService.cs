﻿using System;
using System.Collections.Generic;
using System.Text;
using ZombieSources.Core.Entities;
using ZombieSources.Core.Enums;
using ZombieSources.Core.ViewModels;

namespace ZombieSources.Core.Interfaces.Services
{
    public interface IRecursoMovimentoService
    {
        IEnumerable<RecursoMovimento> BuscarTodos();
        IEnumerable<RecursoMovimento> Filtrar(int recursoId, TipoMovimento? tipoMovimento, string responsavel);
        void Cadastrar(RecursoMovimentoCriarViewModel movimento);
    }
}
