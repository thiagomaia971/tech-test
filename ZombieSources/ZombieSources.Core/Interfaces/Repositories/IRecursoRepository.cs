﻿using System;
using System.Collections.Generic;
using System.Text;
using ZombieSources.Core.Entities;

namespace ZombieSources.Core.Interfaces.Repositories
{
    public interface IRecursoRepository : IRepository<Recurso>
    {
    }
}
