﻿using System;
using System.Collections.Generic;
using System.Text;
using ZombieSources.Core.Entities;

namespace ZombieSources.Core.Interfaces.Repositories
{
    public interface IRecursoMovimentoRepository : IRepository<RecursoMovimento>
    {
        IEnumerable<RecursoMovimento> GetAllWithInclude();
        IEnumerable<RecursoMovimento> GetAllWithInclude(Func<RecursoMovimento, bool> predicate);

    }
}
