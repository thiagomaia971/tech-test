﻿using System;
using System.Collections.Generic;
using System.Text;
using ZombieSources.Core.Entities;

namespace ZombieSources.Core.Interfaces.Repositories
{
    public interface IRepository<T> where T: Entity
    {
        IEnumerable<T> GetAll();
        IEnumerable<T> GetAll(Func<T, bool> predicate);
        T GetSingle(int id);
        void Add(T Entity);
        void Update(T Entity);
        void Delete(T Entity);
    }
}
