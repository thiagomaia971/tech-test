import { ZombieSourcesWebPage } from './app.po';

describe('zombie-sources-web App', () => {
  let page: ZombieSourcesWebPage;

  beforeEach(() => {
    page = new ZombieSourcesWebPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
