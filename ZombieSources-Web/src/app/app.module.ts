import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';

import { SharedModule } from "app/_shared/modules/shared/shared.module";
import { NgProgressModule } from 'ngx-progressbar';
import { AppRoutingModule } from "app/app.routing.module";
import { BrowserXhr } from "@angular/http";
import { MyBrowserXhr } from "app/_services/my-browser-xhr.service";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    NavBarComponent,
    HomeComponent
  ],
  imports: [
    BrowserAnimationsModule,
    SharedModule,

    NgProgressModule,

    AppRoutingModule,
  ],
  providers: [
    { provide: BrowserXhr, useClass: MyBrowserXhr }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
