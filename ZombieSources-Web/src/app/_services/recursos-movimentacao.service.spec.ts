import { TestBed, inject } from '@angular/core/testing';

import { RecursosMovimentacaoService } from './recursos-movimentacao.service';

describe('RecursosMovimentacaoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RecursosMovimentacaoService]
    });
  });

  it('should be created', inject([RecursosMovimentacaoService], (service: RecursosMovimentacaoService) => {
    expect(service).toBeTruthy();
  }));
});
