import { TipoMovimento } from 'app/_models/tipo-movimento.enum.model';
import { ToastrService } from 'ngx-toastr';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { RecursoMovimento } from './../_models/recurso-movimento.model';
import { EntityServiceBase } from './entity-service-base';

@Injectable()
export class RecursosMovimentacaoService extends EntityServiceBase<RecursoMovimento>{

  constructor(http: Http, toastr: ToastrService) {
    super("RecursoMovimento", http, toastr)
  }

  public async buscarFiltrados(recursoId: number, tipoMovimento: TipoMovimento, responsavel: string): Promise<Array<RecursoMovimento>> {
    let response;

    try {
      response = await this.http.get(`${this.urlBase}/Filtrar${this.serializeObjToUrl({ recursoId, tipoMovimento, responsavel })}`).toPromise()
    } catch (error) {
      throw new Error(await this.formatarError(error));
    }
    const responseJSON = await this.formatarResponseJSON(response);

    return responseJSON;
  }

}
