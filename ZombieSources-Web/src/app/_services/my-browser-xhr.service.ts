import { Injectable } from '@angular/core';
import { BrowserXhr } from '@angular/http';

import { NgProgressService } from "ngx-progressbar";
import { LoadingButtonService } from "app/_services/loading-button.service";

@Injectable()
export class MyBrowserXhr extends BrowserXhr {

    public currentRequest = 0;

    constructor(public service: NgProgressService, public loadingButtonService: LoadingButtonService) {
        super();
    }

    public build() {
        const xhr = super.build();

        xhr.onload = (evt) => this.done();
        xhr.onerror = (evt) => this.done();
        xhr.onabort = (evt) => this.done();

        xhr.onloadstart = (event) => {
            this.currentRequest++;
            if (!this.service.isStarted()) {
                this.loadingButtonService.onStartLoadingButtons();
                this.service.start();
            }
            // TODO: do some progress magic here
            // if (event.lengthComputable) {
        };

        // TODO: use event information to compute pending
        // xhr.onprogress = (event) => {};

        return xhr;
    }

    public done() {
        this.currentRequest--;
        if (this.currentRequest === 0) {
            this.loadingButtonService.onStopLoadingButtons();
            this.service.done();
        }
    }
}