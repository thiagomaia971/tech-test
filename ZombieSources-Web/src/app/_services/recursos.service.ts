import { ToastrService } from 'ngx-toastr';
import { EntityServiceBase } from './entity-service-base';
import { Injectable } from '@angular/core';
import { Http } from "@angular/http";

import { Recurso } from "app/_models/recurso.model";

@Injectable()
export class RecursosService extends EntityServiceBase<Recurso> {

  constructor(http: Http, toastrService: ToastrService) {
    super("Recursos", http, toastrService);
  }

  public async buscarFiltrados(id: number, descricao: string): Promise<Array<any>> {
    let response;

    try {
      response = await this.http.get(`${this.urlBase}/Filtrar${this.serializeObjToUrl({ id, descricao })}`).toPromise()
    } catch (error) {
      throw new Error(await this.formatarError(error));
    }
    const responseJSON = await this.formatarResponseJSON(response);

    return responseJSON;
  }

}
