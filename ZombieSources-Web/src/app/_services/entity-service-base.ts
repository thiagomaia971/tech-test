import { RequestOptions, Headers, Http } from '@angular/http';
import { ToastrService } from "ngx-toastr";

export class EntityServiceBase<T> {

    protected urlBase: string;

    constructor(url: string, protected http: Http, protected toastrService: ToastrService) {
        // this.urlBase = `http://localhost:52097/api/${url}`;
        this.urlBase = `http://zombiesourcesapi.azurewebsites.net/api/${url}`;
    }

    public async buscarTodos(): Promise<Array<T>> {
        let response;

        try {
            response = await this.http.get(`${this.urlBase}/Buscar`).toPromise();
        } catch (error) {
            throw new Error(await this.formatarError(error));
        }
        const responseJSON: Array<T> = await this.formatarResponseJSON(response);

        return responseJSON;
    }

    public async buscarPorId(id: string): Promise<T> {
        let response;

        try {
            response = await this.http.get(`${this.urlBase}/BuscarPorId${this.serializeObjToUrl({ id })}`).toPromise();
        } catch (error) {
            throw new Error(await this.formatarError(error));
        }
        const responseJSON: T = await this.formatarResponseJSON(response);

        return responseJSON;
    }

    public async editar(id: string, entity: T): Promise<any> {
        let response;

        try {
            response = await this.http.put(`${this.urlBase}/Editar${this.serializeObjToUrl({ id })}`, entity).toPromise();
        } catch (error) {
            throw new Error(await this.formatarError(error));
        }
        const responseJSON: any = await this.formatarResponseJSON(response);

        return responseJSON;
    }

    public async deletar(id: string): Promise<any> {
        let response;

        try {
            response = await this.http.delete(`${this.urlBase}/Deletar${this.serializeObjToUrl({ id })}`).toPromise();
        } catch (error) {
            throw new Error(await this.formatarError(error));
        }
        const responseJSON: any = await this.formatarResponseJSON(response);

        return responseJSON;
    }

    public async cadastrar(entity: T): Promise<any> {
        let response;

        try {
            response = await this.http.post(`${this.urlBase}/Cadastrar`, entity).toPromise();
        } catch (error) {
            throw new Error(await this.formatarError(error));
        }

        const responseJSON: T = await this.formatarResponseJSON(response);
        return responseJSON;
    }

    protected async formatarResponseJSON(response: any): Promise<any> {
        let responseTxt = await response.text();

        let responseJSON = responseTxt ? await JSON.parse(responseTxt) : {}
        return responseJSON;
    }

    protected async formatarError(error: any): Promise<string> {
        let messageError = "Não foi possível se comunicar com o servidor.";

        if (error.status != 0) {
            messageError = error._body
        }

        this.toastrService.error(messageError);
        return messageError;
    }

    public serializeObjToUrl(obj) {
        return '?' + Object.keys(obj).reduce(function (a, k) {
            if (obj[k] != null || obj[k] != undefined)
                a.push(k + '=' + encodeURIComponent(obj[k]));
            return a
        }, []).join('&')
    }
}