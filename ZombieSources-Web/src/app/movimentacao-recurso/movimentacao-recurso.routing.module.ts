import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MovimentacoesRecursosComponent } from './movimentacoes-recursos/movimentacoes-recursos.component';
import { MovimentarRecursoComponent } from "app/movimentacao-recurso/movimentacoes-recursos/movimentar-recurso/movimentar-recurso.component";


const routes: Routes = [
    { path: "", component: MovimentacoesRecursosComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MovimentacaoRecursoRoutingModule { }