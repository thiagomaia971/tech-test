import { RecursosMovimentacaoService } from './../../../_services/recursos-movimentacao.service';
import { RecursoMovimento } from './../../../_models/recurso-movimento.model';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit, Input, OnChanges, SimpleChanges, ViewChild, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-movimentacao-recurso',
  templateUrl: './movimentar-recurso.component.html',
  styleUrls: ['./movimentar-recurso.component.css']
})
export class MovimentarRecursoComponent implements OnInit, OnChanges {

  @Input() open: boolean;
  @Output() close: EventEmitter<void> = new EventEmitter<void>();
  @ViewChild("modalMovimentarConfirmacao") modal: ModalDirective;

  public movimentacaoForm: FormGroup;
  public movimentacaoRecurso: RecursoMovimento;

  constructor(public fb: FormBuilder, public toastrService: ToastrService, public movimentacaoService: RecursosMovimentacaoService) {
    this.movimentacaoRecurso = new RecursoMovimento();
    this.iniciarForm();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.open.currentValue) {
      this.movimentacaoRecurso = new RecursoMovimento();
      this.iniciarForm();
      this.modal.show();
    } else {
      this.modal.hide();
    }
  }

  ngOnInit() {
    this.modal.onHidden.subscribe(() => this.close.emit());
  }

  public async onSubmit() {
    this.movimentacaoRecurso = this.prepararMovimento();

    await this.movimentacaoService.cadastrar(this.movimentacaoRecurso);
    this.toastrService.success("Movimento gravado com sucesso.");
    this.modal.hide();
  }

  public prepararMovimento(): RecursoMovimento {
    const formModel = this.movimentacaoForm.value;

    const { recursoId, tipoMovimento, quantidade, responsavel } = formModel;

    const movimento: RecursoMovimento = {
      id: 0,
      recursoId,
      tipoMovimento,
      quantidade,
      responsavel,
      data: new Date()
    };

    return movimento;
  }

  public iniciarForm() {
    this.movimentacaoForm = this.fb.group({
      recursoId: [this.movimentacaoRecurso.recursoId, Validators.required],
      tipoMovimento: [this.movimentacaoRecurso.tipoMovimento, Validators.required],
      quantidade: [this.movimentacaoRecurso.quantidade, Validators.required],
      responsavel: [this.movimentacaoRecurso.responsavel, Validators.required]
    });
  }

}
