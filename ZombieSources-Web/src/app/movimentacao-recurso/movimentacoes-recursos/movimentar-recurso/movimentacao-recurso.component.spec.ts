import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovimentacaoRecursoComponent } from './movimentacao-recurso.component';

describe('MovimentacaoRecursoComponent', () => {
  let component: MovimentacaoRecursoComponent;
  let fixture: ComponentFixture<MovimentacaoRecursoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovimentacaoRecursoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovimentacaoRecursoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
