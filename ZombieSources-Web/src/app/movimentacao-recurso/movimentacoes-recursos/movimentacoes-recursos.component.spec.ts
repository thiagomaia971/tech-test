import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovimentacoesRecursosComponent } from './movimentacoes-recursos.component';

describe('MovimentacoesRecursosComponent', () => {
  let component: MovimentacoesRecursosComponent;
  let fixture: ComponentFixture<MovimentacoesRecursosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovimentacoesRecursosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovimentacoesRecursosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
