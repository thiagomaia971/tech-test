import { TipoMovimento } from 'app/_models/tipo-movimento.enum.model';
import { RecursosMovimentacaoService } from './../../_services/recursos-movimentacao.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

import { RecursoMovimento } from './../../_models/recurso-movimento.model';
import { FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-movimentacoes-recursos',
  templateUrl: './movimentacoes-recursos.component.html',
  styleUrls: ['./movimentacoes-recursos.component.css']
})
export class MovimentacoesRecursosComponent implements OnInit {

  public movimentacaoForm: FormGroup;
  public movimentacaoRecurso: RecursoMovimento;
  public movimentos: RecursoMovimento[];

  public _TipoMovimento = TipoMovimento;
  public movimentarModalOpen: boolean;

  constructor(public fb: FormBuilder, public toastrService: ToastrService, public movimentacaoService: RecursosMovimentacaoService) {
    this.movimentacaoRecurso = new RecursoMovimento();
    this.movimentos = [];
    this.iniciarForm();
  }

  ngOnInit() {
  }

  public async onBuscar() {
    this.movimentos = [];
    this.movimentacaoRecurso = this.prepararMovimento();

    if (!(this.movimentacaoRecurso.recursoId || this.movimentacaoRecurso.tipoMovimento || this.movimentacaoRecurso.responsavel))
      this.movimentos = await this.movimentacaoService.buscarTodos();
    else
      this.movimentos = await this.movimentacaoService.buscarFiltrados(this.movimentacaoRecurso.recursoId, this.movimentacaoRecurso.tipoMovimento, this.movimentacaoRecurso.responsavel);
  }

  public onNovo() {
    this.movimentarModalOpen = true;
  }

  public async onModalMovimentarClose() {
    this.movimentarModalOpen = false;
    await this.onBuscar();
  }

  public prepararMovimento(): RecursoMovimento {
    const formModel = this.movimentacaoForm.value;

    const { recursoId, tipoMovimento, quantidade, responsavel } = formModel;

    const movimento: RecursoMovimento = {
      id: 0,
      recursoId,
      tipoMovimento,
      quantidade,
      responsavel,
      data: new Date()
    };

    return movimento;
  }

  public iniciarForm() {
    this.movimentacaoForm = this.fb.group({
      recursoId: [this.movimentacaoRecurso.recursoId],
      tipoMovimento: [this.movimentacaoRecurso.tipoMovimento],
      quantidade: [this.movimentacaoRecurso.quantidade],
      responsavel: [this.movimentacaoRecurso.responsavel]
    });
  }

}
