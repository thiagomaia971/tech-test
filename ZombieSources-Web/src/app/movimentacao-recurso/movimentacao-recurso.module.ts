import { MovimentarRecursoComponent } from './movimentacoes-recursos/movimentar-recurso/movimentar-recurso.component';
import { NgModule } from '@angular/core';


import { SharedModule } from "app/_shared/modules/shared/shared.module";
import { MovimentacaoRecursoRoutingModule } from './movimentacao-recurso.routing.module';
import { MovimentacoesRecursosComponent } from './movimentacoes-recursos/movimentacoes-recursos.component';

import { RecursosMovimentacaoService } from './../_services/recursos-movimentacao.service';

@NgModule({
  imports: [
    SharedModule,
    MovimentacaoRecursoRoutingModule,

  ],
  declarations: [
    MovimentarRecursoComponent,
    MovimentacoesRecursosComponent
  ],
  providers: [
    RecursosMovimentacaoService
  ]
})
export class MovimentacaoRecursoModule { }
