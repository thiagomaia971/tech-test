import { Component, OnInit, Input, ViewChild, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ModalDirective } from "ngx-bootstrap/modal";

import { ToastrService } from 'ngx-toastr';
import { RecursosService } from './../../_services/recursos.service';

import { Recurso } from "app/_models/recurso.model";

@Component({
  selector: 'app-recurso-detalhe',
  templateUrl: './recurso-detalhe.component.html',
  styleUrls: ['./recurso-detalhe.component.css']
})
export class RecursoDetalheComponent implements OnInit {

  public id: string;
  public recursoForm: FormGroup;
  public recursoSelecionado: Recurso;
  public isVisualize: boolean;

  @ViewChild("modalSairConfirmacao") modalSairConfirmacao: ModalDirective;

  constructor(public route: ActivatedRoute,
    public recursoService: RecursosService,
    public router: Router,
    public fb: FormBuilder,
    public toastrService: ToastrService) {
    this.recursoSelecionado = new Recurso();

    this.carregarForm();
  }

  public async ngOnInit() {

    this.route.params.subscribe(async (params: any) => {
      this.id = params['id'];

      if (this.id)
        this.recursoSelecionado = await this.recursoService.buscarPorId(this.id)

      if (!this.recursoSelecionado)
        this.recursoSelecionado = new Recurso();

      this.carregarForm();

      this.isVisualize = true;
    });

  }

  public onBack() {
    if (this.recursoForm.dirty)
      this.modalSairConfirmacao.show();
    else
      this.router.navigate([`/Recursos`]);
  }

  public async onSubmit() {
    if (!this.recursoForm.valid || this.recursoForm.pristine) {
      console.log("Form não valido ou pristine");
      return;
    }

    const recurso = this.prepararRecurso();
    let retorno = null;

    if (this.id)
      retorno = await this.recursoService.editar(this.id, recurso);
    else
      retorno = await this.recursoService.cadastrar(recurso);

    this.toastrService.success(`Recurso ${this.recursoForm.get('descricao').value} ${(this.id) ? "Atualizado" : "Cadastrado"} com sucesso!`);
    this.router.navigate([`/Recursos`]);

  }

  public async onDelete() {
    await this.recursoService.deletar(this.id);
    this.toastrService.success(`Recurso ${this.nomeCompleto} deletado com sucesso!`);
    this.router.navigate([`/Recursos`]);
  }

  get nomeCompleto(): string {
    return `#${(this.id) ? this.id : this.recursoForm.get('id').value} - ${this.recursoForm.get('descricao').value}`;
  }

  public prepararRecurso(): Recurso {
    const formModel = this.recursoForm.value;

    const { id, descricao, observacao, quantidade } = formModel;

    const recurso: Recurso = {
      id: 0,
      descricao,
      observacao,
      quantidade
    };

    return recurso;
  }

  public carregarForm() {
    this.recursoForm = this.fb.group({
      id: [this.recursoSelecionado.id, Validators.required],
      descricao: [this.recursoSelecionado.descricao, Validators.required],
      quantidade: [this.recursoSelecionado.quantidade, Validators.required],
      observacao: [this.recursoSelecionado.observacao]
    })
  }

}
