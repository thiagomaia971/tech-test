import { RecursoRoutingModule } from './recurso.routing.module';
import { NgModule } from '@angular/core';

import { SharedModule } from './../_shared/modules/shared/shared.module';
import { RecursosComponent } from './recursos/recursos.component';

import { RecursosService } from './../_services/recursos.service';
import { RecursoDetalheComponent } from './recurso-detalhe/recurso-detalhe.component';

@NgModule({
  imports: [
    SharedModule,
    RecursoRoutingModule
  ],
  declarations: [
    RecursosComponent,
    RecursoDetalheComponent
  ],
  providers: [
    RecursosService
  ]
})
export class RecursoModule { }
