import { Recurso } from 'app/_models/recurso.model';
import { Component, OnInit } from '@angular/core';

import { RecursosService } from './../../_services/recursos.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-recursos',
  templateUrl: './recursos.component.html',
  styleUrls: ['./recursos.component.css']
})
export class RecursosComponent implements OnInit {

  public columns: Array<any> = [
    { title: '#', name: 'id', filtering: { filterString: '', placeholder: 'Filtrar pelo id' } },
    { title: 'Descricao', name: 'descricao', filtering: { filterString: '', placeholder: 'Filtrar pela descrição' } },
    { title: 'Quantidade', name: 'quantidade', filtering: { filterString: '', placeholder: 'Filtrar pela quantidade' } },
    { title: 'Observação', name: 'observacao', filtering: { filterString: '', placeholder: 'Filtrar pela observação' } },
  ];

  public recursos: Array<Recurso> = [];
  public recursoSearch: Recurso = new Recurso();
  public recursoSelecionado: Recurso;

  constructor(public recursoService: RecursosService, public router: Router) { }

  public async ngOnInit() {
  }

  public async onBuscar() {
    this.recursos = [];

    if (!(this.recursoSearch.id || this.recursoSearch.descricao))
      this.recursos = await this.recursoService.buscarTodos();
    else
      this.recursos = await this.recursoService.buscarFiltrados(this.recursoSearch.id, this.recursoSearch.descricao);
  }

  public async onNovo() {
    this.router.navigate([`/Recursos/Cadastro`]);    
  }

  public handleRowClick(row: Recurso) {
    this.router.navigate([`/Recursos/Detalhe/${row.id}`]);
    //this.recursoSelecionado = row;
  }

  public handleModalHide() {
    this.recursoSelecionado = null;
  }

}
