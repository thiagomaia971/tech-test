import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RecursosComponent } from './recursos/recursos.component';
import { RecursoDetalheComponent } from './recurso-detalhe/recurso-detalhe.component';

const routes: Routes = [
    { path: "", component: RecursosComponent },
    { path: "Detalhe/:id", component: RecursoDetalheComponent },
    { path: "Cadastro", component: RecursoDetalheComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RecursoRoutingModule { }