import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule, BrowserXhr } from "@angular/http";

import { LoadingButtonService } from 'app/_services/loading-button.service';
import { LaddaModule } from 'angular2-ladda';

import { Ng2TableModule, NgTableComponent, NgTableFilteringDirective, NgTablePagingDirective, NgTableSortingDirective } from 'ng2-table/ng2-table';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TableBaseComponent } from "app/table-base/table-base.component";

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/switchMap';
import { MyBrowserXhr } from "app/_services/my-browser-xhr.service";
import { LoadingButtonDirective } from "app/_directives/loading-button/loading-button.directive";
import { ModalModule } from 'ngx-bootstrap/modal';
import { ToastrModule } from 'ngx-toastr';
import { BrowserModule } from "@angular/platform-browser";

@NgModule({
  imports: [
    //,
    //BrowserModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    Ng2TableModule,
    PaginationModule.forRoot(),
    ModalModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 7000,
      positionClass: 'toast-bottom-right',
      extendedTimeOut: 5000,
      progressBar: true
    }),
    LaddaModule.forRoot({
      style: "zoom-in",
      spinnerColor: "rgb(204, 24, 30)",
      spinnerSize: 40,
      spinnerLines: 12
    }),
  ],
  exports: [
    //BrowserAnimationsModule,
    //BrowserModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    Ng2TableModule,
    PaginationModule,
    ModalModule,
    ToastrModule,
    //LaddaModule,

    TableBaseComponent,
    NgTableComponent,
    
    LoadingButtonDirective,
    /*ButtonSearchDirective,*/
    NgTableFilteringDirective,
    NgTablePagingDirective,
    NgTableSortingDirective
  ],
  declarations: [
    LoadingButtonDirective,
    /*ButtonSearchDirective,*/

    TableBaseComponent,
  ],
  providers: [
    LoadingButtonService,
    { provide: BrowserXhr, useClass: MyBrowserXhr }
  ]
})
export class SharedModule { }
