import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
    { path: "", component: HomeComponent },
    { path: "Recursos", loadChildren: "app/recurso/recurso.module#RecursoModule" },
    { path: "MovimentacaoRecursos", loadChildren: "app/movimentacao-recurso/movimentacao-recurso.module#MovimentacaoRecursoModule" },
    { path: "**", component: PageNotFoundComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }