export class Recurso {
    public id: number = 0;
    public descricao: string = null;
    public quantidade: number = 0;
    public observacao: string = null;
}