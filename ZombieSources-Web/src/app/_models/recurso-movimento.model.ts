import { TipoMovimento } from "app/_models/tipo-movimento.enum.model";

export class RecursoMovimento {
    public id: number;
    public recursoId: number;
    public tipoMovimento: TipoMovimento;
    public responsavel: string;
    public quantidade: number;
    public data: Date;

}